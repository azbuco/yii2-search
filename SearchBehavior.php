<?php

namespace azbuco\search;

use yii\base\Behavior;
use yii\db\ActiveQuery;
use yii\validators\Validator;

class SearchBehavior extends Behavior {

    public $q;

    public function attach($owner)
    {
        parent::attach($owner);

        $owner->validators[] = Validator::createValidator('safe', $this->owner, 'q');
    }

    /**
     * @param ActiveQuery $query
     * @param array $attributes
     */
    public function searchFilter($query, $attributes = [])
    {
        if (empty($this->q) || empty($attributes)) {
            return;
        }

        $parts = explode(' ', $this->q);
        foreach ($parts as $part) {
            $conditions = ['or'];
            foreach ($attributes as $attribute) {
                $conditions[] = ['like', $attribute, $part];
            }
            $query->andWhere($conditions);
        }
    }

}
