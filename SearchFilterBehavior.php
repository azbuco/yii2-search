<?php

namespace azbuco\search;

use yii\base\Behavior;

class SearchFilterBehavior extends Behavior {
    
    /**
     * @param string $expression
     * @param array $attributes
     */
    public function searchFilter($expression = '', $attributes = [])
    {
        if (empty($expression) || empty($attributes)) {
            return $this->owner;
        }

        $parts = explode(' ', $expression);
        foreach ($parts as $part) {
            $conditions = ['or'];
            foreach ($attributes as $attribute) {
                $conditions[] = ['like', $attribute, $part];
            }
            $this->owner->andWhere($conditions);
        }
        
        return $this->owner;
    }
    
}
