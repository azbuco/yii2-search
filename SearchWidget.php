<?php

namespace azbuco\search;

use yii\helpers\Html;
use yii\widgets\InputWidget;

class SearchWidget extends InputWidget {

    public $template = '<div class="form-group-feedback form-group-feedback-right">{input}'
    . '<div class="form-control-feedback">'
    . '<i class="icon-search4 font-size-base text-muted"></i>'
    . '</div></div>';

    public function init()
    {
        parent::init();

        Html::addCssClass($this->options, 'form-control');
    }

    public function run()
    {
        if ($this->hasModel()) {
            $input = Html::activeTextInput($this->model, $this->attribute, $this->options);
        } else {
            $input = Html::textInput($this->name, $this->value, $this->options);
        }

        echo str_replace('{input}', $input, $this->template);
    }

}
