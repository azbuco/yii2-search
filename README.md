# Simple search widget #

## Usage ##

### 1. Add the behavior to the search model. ###

In your search model:

```
#!php
<?php
// ...
public function behaviors()
{
    return [
        // ...
        'searchBehavior' => [
            'class' => \azbuco\search\SearchBehavior::class,
        ],
    ];
}
// ...

```

### 2. Filtery your search.  ###

In your search model:

```
#!php
<?php
// ...
public function search($params) {
    // $query = ...
    // ...
    $this->searchFilter($query, ['fist_name', 'last_name', 'city', 'postal_code']);
}
// ...

```

### 3. Add the field to the view. ###

```
#!php
<?php
// ...
$form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
]);

echo $form->field($model, 'q')->widget(SearchWidget::class, ['options' => ['placeholder' => 'Search...']])->label(false);

ActiveForm::end();
// ...

```